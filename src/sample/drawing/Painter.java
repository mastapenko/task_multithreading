package sample.drawing;

import javafx.application.Platform;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import sample.Dimens;

public class Painter {

    public static Pane pane;

    public static void paintCash(GraphicsContext gc, int x, int y, int count){
        for(int i = 0; i < count; i++) {
            gc.strokeRoundRect(x + i* Dimens.WIDTH_CASH, y, 50, 200, 5, 5);
            gc.fillRoundRect(x + 100 + i*Dimens.WIDTH_CASH, y, 20, 200, 5, 5);
            gc.fillRoundRect(x + 60 + i*Dimens.WIDTH_CASH, y+150, 40, 40, 5, 5);
        }
    }

    public static void setPane(Pane pane){
        Painter.pane = pane;
    }

    public static void removeFromPane(Circle circle){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                pane.getChildren().remove(circle);
            }
        });
    }
}
