package sample.units;

import sample.State;

import java.util.ArrayList;
import java.util.List;

public class Shop{

    private final List<Cashier> cashes;

    public Shop(int count){
        cashes = new ArrayList<>();
        for(int i = 0; i < count; i++){
            Cashier c = new Cashier(i);
            Thread t = new Thread(c);
            t.setDaemon(true);
            t.start();
            cashes.add(c);
        }
    }

    public void checkOnNotBusyCash(Buyer b){
        Cashier theBestCash = null;
        synchronized (cashes) {
            theBestCash = cashes.get(0);
            for (int i = 1; i < cashes.size(); i++) {
                if (cashes.get(i).isHavePlace() && cashes.get(i).getCountBuyers() < theBestCash.getCountBuyers()) {
                    theBestCash = cashes.get(i);
                }
            }
        }
        theBestCash.addBuyer(b);
        b.setCash(theBestCash);
        b.setState(State.WALK_TO_QUEUE);
    }

}
