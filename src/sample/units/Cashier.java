package sample.units;

import sample.State;
import java.util.*;

import static sample.State.WAIT;

public class Cashier implements Runnable{

    private State state;
    private int number;
    private volatile int timeOnProccess;
    private List<Buyer> buyersInQueue;

    public Cashier(int number) {
        this.number = number;
        buyersInQueue = new ArrayList<>();
        state = WAIT;
    }

    @Override
    public void run() {
        while(true) {
            switch (state) {
                case WAIT:
                    break;
                case PROCCESS_BUYER: {
                    if (buyersInQueue.size() == 0) {
                        state = WAIT;
                        break;
                    }
                    if (timeOnProccess != 0) {
                        timeOnProccess--;
                    } else {
                        System.out.println();
                        Buyer buyer = buyersInQueue.get(0);
                        buyersInQueue.remove(buyer);
//                        System.out.println(Calendar.getInstance().getTime().toString() + " Cash " + Thread.currentThread().getId() + " remove buyer " + buyer.id);
                        buyer.exit();
                        state = WAIT;
                        for (Buyer b : buyersInQueue){
                            b.moveAhead();
                        }
                    }
                }
                break;
                default:{}
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isHavePlace(){
        return !(buyersInQueue.size() == 5);
    }

    public int getCountBuyers(){
        return buyersInQueue.size();
    }

    public synchronized void addBuyer(Buyer b){
        if (b != null)
            buyersInQueue.add(b);
        else
            throw new NullPointerException();
    }

    public synchronized int getNumberBuyerInQueue(Buyer reqBuyer) {
        int number = -1;
        for (int i = 0; i < buyersInQueue.size(); i++){
            if (buyersInQueue.get(i).equals(reqBuyer)) {
                number = i;
                break;
            }
        }
        return number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public synchronized void setProccesBuyer(){
        if (buyersInQueue.size() != 0 && timeOnProccess == 0) {
            Random rand = new Random();
            timeOnProccess = rand.nextInt() % 10 + 20;
            state = State.PROCCESS_BUYER;
//            System.out.println(Calendar.getInstance().getTime().toString() + " Cash " + Thread.currentThread().getId() + " procces buyer " + (buyersInQueue.size() == 0 ? "null" : buyersInQueue.get(0).id) + " time " + timeOnProccess);
        }
    }
}
