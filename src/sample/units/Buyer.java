package sample.units;

import javafx.animation.AnimationTimer;
import javafx.scene.Parent;
import javafx.scene.shape.Circle;
import sample.Dimens;
import sample.State;
import sample.drawing.Painter;

import static sample.State.*;

public class Buyer implements Runnable {

    //coords
    private Shop shop;
    private Circle circle;
    private double velocity;
    private State state;
    private Cashier cash;
    private AnimationTimer timer;
    public int id;

    public Buyer(Circle circle, double velocity, Shop shop) {
        this.circle = circle;
        this.velocity = velocity;
        this.shop = shop;
        this.timer = new RedrawTimer();
        state = SEARCH;
    }

    @Override
    public void run() {
//        System.out.println("Thread" + Thread.currentThread().getId() + " was started");
        id = (int) Thread.currentThread().getId();
        timer.start();
        while(true){
            switch (state){
                case SEARCH:{
                    shop.checkOnNotBusyCash(this);
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }break;
                case EXIT:{
                    Painter.removeFromPane(circle);
                    return;
                }
                default:{}
            }
        }
    }

    private void stateSearch(){
        if (circle.getCenterX() >= 410 && velocity > 0 || circle.getCenterX() <= 50 && velocity < 0){
            velocity *= -1;
        }
        circle.setCenterX(circle.getCenterX() + velocity);
    }

    private void walkToQueue(){
        //x=35, y=345
        double oldX = circle.getCenterX();
        double oldY = circle.getCenterY();
        double newX = oldX + (Math.abs((35 + cash.getNumber()*Dimens.WIDTH_CASH) - circle.getCenterX()))*(velocity/30);
        double newY = oldY + (Math.abs(305 - circle.getCenterY()))*(velocity/30);
        circle.setCenterX(newX);
        circle.setCenterY(newY);
        if (Math.abs(newX - oldX) < 0.5) {
            oldY = circle.getCenterY();
            newY = oldY + velocity;
            circle.setCenterY(newY);
            if (circle.getCenterY() >= (475 - cash.getNumberBuyerInQueue(this)*40)) {
                state = INQUEUE;
                cash.setProccesBuyer();
            }

        }
    }

    public void moveAhead(){
        state = MOVE_IN_QUEUE;
    }

    private void moveInQueue(){
        double oldY = circle.getCenterY();
        double newY = oldY + velocity;
        circle.setCenterY(newY);
        if (circle.getCenterY() >= (475 - cash.getNumberBuyerInQueue(this)*40)) {
            state = INQUEUE;
        }
    }

    private void walkToExit(){
        double oldY = circle.getCenterY();
        circle.setCenterY(oldY + velocity);
        if (circle.getCenterY() >= Dimens.SCREEN_HEIGHT+Dimens.BUYER_RADIUS) {
            state = EXIT;
            cash.setProccesBuyer();
        }
    }

    public void exit(){
        state = WALK_TO_EXIT;
    }

    @Override
    public String toString() {
        return Thread.currentThread().getName() + " Buyer{" +
                "x=" + circle.getCenterX() +
                ", y=" + circle.getCenterY() +
                '}';
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Cashier getCash() {
        return cash;
    }

    public void setCash(Cashier cash) {
        this.cash = cash;
    }

    private class RedrawTimer extends AnimationTimer{
        @Override
        public void handle(long now) {
            switch (state){
                case SEARCH:{
                    stateSearch();
                }
                break;
                case WALK_TO_QUEUE:
                    walkToQueue();
                    break;
                case INQUEUE:{
                    break;
                }
                case MOVE_IN_QUEUE:{
                    moveInQueue();
                }
                case WALK_TO_EXIT:{
                    walkToExit();
                }break;
            }
        }
    }
}
