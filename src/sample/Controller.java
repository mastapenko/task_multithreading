package sample;

import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import sample.drawing.Painter;
import sample.units.Buyer;
import sample.units.Shop;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

public class Controller implements Initializable{

    private List<Thread> buyersThreads;
    public Pane pane;
    public Canvas canvas;
    Shop shop;
    private int countCashes;
//width x=[50, 410]
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        GraphicsContext gc =  canvas.getGraphicsContext2D();
        countCashes = 3;
        Painter.setPane(pane);
        Painter.paintCash(gc, Dimens.CASHES_X, Dimens.CASHES_Y, countCashes);

        initBuyers();
        initFlowBuyers();
        initCleaning();
    }

    private void initBuyers(){
        Random rand = new Random();

        shop = new Shop(countCashes);
        buyersThreads = new ArrayList<Thread>();
        for(int i = 0; i < 6; i++){
            Circle circle = new Circle(Dimens.BEGIN_BUYER_X_MIN,
                    Dimens.BEGIN_BUYER_Y, Dimens.BUYER_RADIUS, Color.RED);
            pane.getChildren().add(circle);
            buyersThreads.add(new Thread(new Buyer(circle, rand.nextDouble() % 0.1 + 1, shop)));
            buyersThreads.get(i).setDaemon(true);
        }

        for(Thread thrd : buyersThreads){
            thrd.start();
        }
    }

    private void initFlowBuyers(){
        Random rand = new Random();
        Thread creatorByuers = new Thread(() -> {
            do {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Circle circle = new Circle(Dimens.BEGIN_BUYER_X_MIN,
                        Dimens.BEGIN_BUYER_Y, Dimens.BUYER_RADIUS, Color.RED);
                Platform.runLater(() ->{
                    pane.getChildren().add(circle);
                });
                Thread t = new Thread(new Buyer(circle, rand.nextDouble() % 0.1 + 1, shop));
                t.setDaemon(true);
                buyersThreads.add(t);
                t.start();
            }while (true);
        });
        creatorByuers.setDaemon(true);
        creatorByuers.start();
    }

    private void initCleaning() {
        Thread cleaning = new Thread(() -> {
           while(true){
               try {
                   Thread.sleep(5000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }

               for(int i = 0 ; i < buyersThreads.size(); i++){
                   if (!buyersThreads.get(i).isAlive()){
                       buyersThreads.remove(i);
                   }
               }
           }
        });
        cleaning.setDaemon(true);
        cleaning.start();
    }
}
