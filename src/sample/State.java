package sample;

public enum State {
    SEARCH,
    WALK_TO_QUEUE,
    MOVE_IN_QUEUE,
    INQUEUE,
    WALK_TO_EXIT,
    EXIT,

    WAIT,
    PROCCESS_BUYER
}
