package sample;

public class Dimens {
    public static int CASHES_Y = 310;
    public static int CASHES_X = 60;
    public static int WIDTH_CASH = 170;
    public static int BEGIN_BUYER_X_MIN = 20;
    public static int BEGIN_BUYER_X_MAX = 500;
    public static int BEGIN_BUYER_Y = 20;
    public static int BUYER_RADIUS = 15;
    public static int SCREEN_WIDTH = 520;
    public static int SCREEN_HEIGHT = 520;
}
